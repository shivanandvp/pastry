#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ PasteIt - A paste client                                                    │
#│ ========================                                                    │
#│ File   : pasteit/scripts/install_archlinux_package.sh                       │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/pasteit                             │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Description:                                                                │
#│                                                                             │
#│ A script to install an existing Arch Linux package built using the          │
#│ build_archlinux_package.sh script. If a built package does not exist,       │
#│ however, this script will call the build script first and then installs the │
#│ resulting package. All arguments passed to this script will be passed       │
#│ through to `pacman -U`. You do not need to be in the current directory to   │
#│ call this script using a relative or absolute path, unlike how `makepkg`    │
#│ requires you to.                                                            │
#│ -----                                                                       │
#│ Last Modified: Sat, 8th January 2022 4:19:36 PM -06:00                      │
#│ Modified By  :                                                              │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

SCRIPT_DIRECTORY="$(dirname -- "$(readlink -f -- "$0")")"
PROJECT_DIRECTORY="$(dirname -- "$SCRIPT_DIRECTORY")"

if ls "$SCRIPT_DIRECTORY"/archlinux_packaging/*.pkg.tar.* > /dev/null 2>&1;then
    set -o xtrace
    sudo pacman -U "$@" "$SCRIPT_DIRECTORY"/archlinux_packaging/*.pkg.tar.zst
    set +o xtrace
else
    set -o xtrace
    sh "$SCRIPT_DIRECTORY"/build_archlinux_package.sh --install "$@"
    set +o xtrace
fi