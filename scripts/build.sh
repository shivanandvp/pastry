#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ PasteIt - A paste client                                                    │
#│ ========================                                                    │
#│ File   : pasteit/.gitlab-ci.yml                                             │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/pasteit                             │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Description:                                                                │
#│                                                                             │
#│ A script to build the library and binary crates using cargo build. All      │
#│ arguments passed to this script will be passed-through to `cargo build -- ` │
#│ -----                                                                       │
#│ Last Modified: Sat, 8th January 2022 4:18:33 PM -06:00                      │
#│ Modified By  :                                                              │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

SCRIPT_DIRECTORY="$(dirname -- "$(readlink -f -- "$0")")"
PROJECT_DIRECTORY="$(dirname -- "$SCRIPT_DIRECTORY")"

( # Create subshell to nullify directory changes on exit
    # Run makepkg
    set -o xtrace
    cd "$PROJECT_DIRECTORY" && \
    cargo build -- "$@"
    set +o xtrace
)