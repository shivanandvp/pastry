#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ PasteIt - A paste client                                                    │
#│ ========================                                                    │
#│ File   : pasteit/.gitlab-ci.yml                                             │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/pasteit                             │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Description:                                                                │
#│                                                                             │
#│ A script to build an Arch Linux package using the PKGBUILD found under      │
#│ ./archlinux_packaging. All arguments passed to this script will be passed   │
#│ through to `makepkg`. You do not need to be in the current directory to     │
#│ call this script using a relative or absolute path, unlike how `makepkg`    │
#│ requires you to.                                                            │
#│ -----                                                                       │
#│ Last Modified: Sat, 8th January 2022 4:18:33 PM -06:00                      │
#│ Modified By  :                                                              │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

SCRIPT_DIRECTORY="$(dirname -- "$(readlink -f -- "$0")")"
PROJECT_DIRECTORY="$(dirname -- "$SCRIPT_DIRECTORY")"

( # Create subshell to nullify directory changes on exit
    # Run makepkg
    set -o xtrace
    cd "$SCRIPT_DIRECTORY"/archlinux_packaging && \
    makepkg \
        --force \
        --syncdeps \
        "$@"
    set +o xtrace
)